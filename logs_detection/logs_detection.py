import numpy as np
import argparse
import cv2

image = cv2.imread('Troncos/WIN_20160621_110948.JPG')
output = image.copy()
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


gblur = cv2.GaussianBlur(image,(7,7),1) #1,1 1
ggblur = cv2.cvtColor(gblur, cv2.COLOR_BGR2GRAY)
edges = cv2.Canny(gblur,10,100)
edgesbgr = cv2.cvtColor(edges, cv2.COLOR_GRAY2BGR)

# detectar circulos
circles = cv2.HoughCircles(edges, cv2.HOUGH_GRADIENT, 4,60,maxRadius=45)

if circles is not None:
	# convertir (x, y) y radio de circulos a enteros
	circles = np.round(circles[0, :]).astype("int")
	print(circles[:2])
	#recorrer x,y y radio en los circulos
	print(len(circles))
	for (x, y, r) in circles:
		# draw the circle in the output image, then draw a rectangle
		# corresponding to the center of the circle
		cv2.circle(output, (x, y), r, (0, 255, 0), 4)
		cv2.rectangle(output, (x - 5, y - 5), (x + 5, y + 5), (0, 128, 255), -1)	

	# show the output image
	#cv2.imshow("output", np.hstack([image, output]))

text = "{1:d} {0}".format("Troncos detectados ",int(len(circles)))
cv2.putText(output, text, (50,50), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 3)

out2 = np.hstack([edgesbgr, output])
cv2.namedWindow('r', cv2.WINDOW_NORMAL)
cv2.imshow("r",output)
cv2.imshow("r", out2)
#cv2.imwrite("troncos_detectados2.jpg",output)
#cv2.imwrite("troncos_detectados2_conbordes.jpg",out2)

cv2.waitKey(0)
